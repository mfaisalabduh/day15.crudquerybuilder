@extends('adminlte.master')

@section('content')
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Edit cast {{$cast00->id}}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/cast00/{{$cast00->id}}" method="POST">
            @csrf
            @method('PUT')
        <div class="card-body">
            <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" id="nama" name="nama" value="{{old('nama', $cast00->nama)}}" placeholder="Masukkan nama cast">
            @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
            <div class="form-group">
            <label for="umur">Umur</label>
            <input type="number" class="form-control" id="umur" name="umur" value="{{old('umur', $cast00->umur)}}"placeholder="Masukkan umur">
            @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
            <div class="form-group">
            <label for="bio">Bio</label>
            <input type="text" class="form-control" id="bio" name="bio" value="{{old('bio', $cast00->bio)}}"placeholder="Tuliskan bio singkat mengenai cast bersangkutan.">
            @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
    
        </div>
        <!-- /.card-body -->
    
        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Create</button>
        </div>
        </form>
    </div>

@endsection