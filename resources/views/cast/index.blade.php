@extends('adminlte.master')

@section('content')
    <div class="">
    <div class="card">
        <div class="card-header">
        <h3 class="card-title">Cast Table</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if(session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
            @endif
            <a class="btn btn-primary mb-2" href="/cast/create">Create new cast</a>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th style="width: 10px">#</th>
                <th>Nama</th>
                <th>Umur</th>
                <th>Bio</th>
                <th style="width: 10px">Action</th>
            </tr>
            </thead>
            <tbody>
                @forelse($cast as $key => $cast00) 
                    <tr>
                        <td> {{$key + 1}} </td>
                        <td> {{$cast00->nama}} </td>
                        <td> {{$cast00->umur}} </td>
                        <td> {{$cast00->bio}} </td>
                        
                        <td style="display: flex;">
                            <a href="/cast00/{{$cast00->id}}" class="btn btn-info btn-sm">Show</a>
                            <a href="/cast00/{{$cast00->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                            <form action="/cast00/{{$cast00->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                            </form>
                        </td>
                    </tr>
                    @empty
                        <tr>
                            <td colspan = "5" align= "center">No cast appear yet.</td>
                        </tr>
                @endforelse
            <!-- <tr>
                <td>1.</td>
                <td>Update software</td>
                <td>
                <div class="progress progress-xs">
                    <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
                </div>
                </td>
                <td><span class="badge bg-danger">55%</span></td>
            </tr> -->
            
            </tbody>
        </table>
        </div>
        <!-- /.card-body -->
        <!-- <div class="card-footer clearfix">
        <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">«</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">»</a></li>
        </ul>
        </div> -->
    </div>

    </div>
@endsection