@extends('adminlte.master')

@section('content')
    <div class="">
    <div class="card">
        <div class="card-header">
        <h3 class="card-title">Posts Table</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if(session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
            @endif
            <a class="btn btn-primary mb-2" href="/posts/create">Create new post</a>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th style="width: 10px">#</th>
                <th>Title</th>
                <th>Body</th>
                <th style="width: 40px">Action</th>
            </tr>
            </thead>
            <tbody>
                @forelse($posts as $key => $post)
                    <tr>
                        <td> {{$key + 1}} </td>
                        <td> {{$post->title}} </td>
                        <td> {{$post->body}} </td>
                        <td style="display: flex;">
                            <a href="/post/{{$post->id}}" class="btn btn-info btn-sm">Show</a>
                            <a href="/post/{{$post->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                            <form action="/post/{{$post->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                            </form>
                        </td>
                    </tr>
                    @empty
                        <tr>
                            <td colspan = "4" align= "center">No post appear yet.</td>
                        </tr>
                @endforelse
            <!-- <tr>
                <td>1.</td>
                <td>Update software</td>
                <td>
                <div class="progress progress-xs">
                    <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
                </div>
                </td>
                <td><span class="badge bg-danger">55%</span></td>
            </tr> -->
            
            </tbody>
        </table>
        </div>
        <!-- /.card-body -->
        <!-- <div class="card-footer clearfix">
        <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">«</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">»</a></li>
        </ul>
        </div> -->
    </div>

    </div>
@endsection